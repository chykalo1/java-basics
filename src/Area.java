import java.util.Random;

public class Area {
    public double calculate(int n) {
        if (n % 2 == 0) {
            return Math.pow(n, 2); // square
        } else if (n % 3 == 0) {
            return Math.PI * Math.pow(n, 2); // circle
        } else {
            return (Math.sqrt(3) / 4) * Math.pow(n, 2); // triangle
        }
    }

    public static void main(String[] args) {
        Area a = new Area();
        Random random = new Random();
        int i = random.nextInt(10) + 1;
        System.out.println("N: " + i);
        System.out.println("Area: " + a.calculate(i));
    }
}
